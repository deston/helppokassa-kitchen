const dotenv = require('dotenv')
const request = require('request')
const querystring = require('querystring')
const escpos = require('escpos');

dotenv.config();

var form = {
    store_id: `${process.env.STORE_ID}`
};

var formData = querystring.stringify(form);
var contentLength = formData.length;

//Setup printer
const device  = new escpos.USB();
const options = {encoding: "437"}
const printer = new escpos.Printer(device, options);
device.open(function(){
  request({
      headers: {
        'Content-Length': contentLength,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Apikey': `${process.env.API_KEY}`
      },
      uri: `${process.env.END_POINT}`,
      body: formData,
      method: 'POST'
    }, function (err, res, body) {
      var receipt_data = JSON.parse(body);

	console.log(receipt_data);

	if (receipt_data.length < 1){
		device.close();
		return;
	}

      for (var i = 0, len = receipt_data.length; i < len; i++) {
          if (receipt_data[i] != "") {
            printer.text(JSON.parse(receipt_data[i]).receipt_48).cut().close();
          }
        }
  });
});

//device.close();
